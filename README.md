
# Image Gallery
  
 

## Installation
To install locally the project please run the following command inside the folder
```bash
npm install
```

## Dev
To work with the project only need to run the following command
```bash
npm run dev
```
and navigate to [http://localhost:5173](http://localhost:5173)

## Build
To build a release version you need to run the following command
```bash
npm run build
```
after the command finish you will find on the dist folder all the file necessary to deploy the app