import {AlertDialog, Box, Button, Card, Container, Flex, Text} from '@radix-ui/themes';
import axios from 'axios';
import {ArrowLeftIcon, StarFilledIcon, StarIcon} from '@radix-ui/react-icons';
import {useQuery} from '@tanstack/react-query';
import {useHistory, useParams} from 'react-router-dom';
import {saveAs} from 'file-saver';
import {Loading} from '../shared/components/loader';
import {useUserStore} from '../store/userStore';


const fetchDetails = async (id: string) => {
    const response = await axios.get(`https://picsum.photos/id/${id}/info`);
    return response.data;
};

type DetailsParams = {
    id: string;
}


const Details = () => {
    const {id} = useParams<DetailsParams>();
    const {goBack} = useHistory();
    const {checkImage, saveImage, removeImage} = useUserStore();
    const {data, status, error} = useQuery({queryKey: ['details', id], queryFn: () => fetchDetails(id)});
    const isFavorite = checkImage(data);
    const onFavoriteClick = (e: { preventDefault: () => void; }) => {
        e.preventDefault();
        if (isFavorite) {
            removeImage(data);
        } else {
            saveImage(data);
        }
    };
    const onDownloadClick = () => {
        saveAs(`${data.download_url}.jpg`, `${data.author}.jpg`);
    }


    return status === 'pending' ? (
        <Loading/>
    ) : status === 'error' ? (
            <p>Error: {error.message}</p> //TODO CREATE ERROR BOUNDARY
        ) :
        (
            <Container>
                <Flex gap="9" justify="between">
                    <Button onClick={goBack}>
                        <ArrowLeftIcon width="16" height="16"/>
                    </Button>
                    <Flex direction="row" align="center" justify="center" gap="6" wrap="wrap">
                        <Card size="3">
                            <Flex direction="column" gap="3">
                                <Box>
                                    <Flex direction="row" gap="3" justify="between">
                                        <Text as="div" size="6" weight="bold">
                                            {data.author}
                                        </Text>
                                        {isFavorite ? (
                                            <AlertDialog.Root>
                                                <AlertDialog.Trigger>
                                                    <Button>
                                                        <StarFilledIcon width="16" height="16"/>
                                                    </Button>
                                                </AlertDialog.Trigger>
                                                <AlertDialog.Content style={{maxWidth: 500}}>
                                                    <AlertDialog.Title>Quitar de favoritos</AlertDialog.Title>
                                                    <Flex direction="column" gap="4">
                                                        <AlertDialog.Description size="2">
                                                            ¿Estás seguro de que deseas eliminar esta imagen de
                                                            favoritos?
                                                        </AlertDialog.Description>
                                                        <Flex gap="3" justify="end">
                                                            <AlertDialog.Cancel>
                                                                <Button variant="soft" color="gray">Cancelar</Button>
                                                            </AlertDialog.Cancel>
                                                            <AlertDialog.Action>
                                                                <Button color="red"
                                                                        onClick={onFavoriteClick}>Eliminar</Button>
                                                            </AlertDialog.Action>
                                                        </Flex>
                                                    </Flex>
                                                </AlertDialog.Content>
                                            </AlertDialog.Root>
                                        ) : (
                                            <Button onClick={onFavoriteClick}>
                                                <StarIcon width="16" height="16"/>
                                            </Button>
                                        )}
                                    </Flex>
                                </Box>
                                <img
                                    src={data.download_url}
                                    alt={`image from ${data.author}`}
                                    style={{
                                        objectFit: 'cover',
                                        width: '100%',
                                        height: '100%',
                                        borderRadius: 'var(--radius-2)',
                                    }}
                                />
                                <Box>
                                    <Flex direction="row" gap="3" justify="between">
                                        <Box>
                                            <Flex direction="row" gap="3">
                                                <Text as="div" size="3" weight="bold">
                                                    Ancho: {data.width}
                                                </Text>
                                                <Text as="div" size="3" weight="bold">
                                                    Alto: {data.height}
                                                </Text>
                                            </Flex>
                                        </Box>
                                        <Button onClick={onDownloadClick}>
                                            Descargar imagen
                                        </Button>
                                    </Flex>
                                </Box>
                            </Flex>
                        </Card>
                    </Flex>
                </Flex>
            </Container>
        )
}

export default Details;