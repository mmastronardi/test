import {Container, Flex, Grid, Heading, ScrollArea} from '@radix-ui/themes';
import {useUserStore} from '../store/userStore';
import ImageCard from '../shared/components/imageCard';
import {Iimage} from '../shared/interfaces/images';

const Favorites = () => {
    const {favorites} = useUserStore();

    return (
        <Container>
            {favorites.length > 0 ? (
                <ScrollArea type="auto" scrollbars="vertical" style={{height: "90vh"}}>
                    <Grid columns="3" gap="3" width="auto">
                        {favorites.map((image: Iimage) => (
                            <ImageCard image={image} key={image.id}/>
                        ))}
                    </Grid>
                </ScrollArea>
            ) : (
                <Flex direction="column" justify="center" className='no-favorites-container'>
                    <Flex justify="center">
                        <Heading>No hay fotos agregadas a favoritos actualmente</Heading>
                    </Flex>
                </Flex>
            )}
        </Container>
    )
};

export default Favorites;