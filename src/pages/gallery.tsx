import axios from 'axios';
import {useMemo} from 'react';
import {useInfiniteQuery} from '@tanstack/react-query';
import {Container} from '@radix-ui/themes';
import {Loading} from '../shared/components/loader';
import ImageList from '../shared/components/imageList';

const fetchGallery = async ({pageParam}: { pageParam: number }) => {
    const response = await axios.get(`https://picsum.photos/v2/list?page=${pageParam}`);
    const link = response.headers.link;
    return {
        data: response.data,
        previousId: link.includes('prev') ? pageParam - 1 : undefined,
        nextId: link.includes('next') ? pageParam + 1 : undefined
    };
};

const Gallery = () => {
    const {
        data,
        error,
        fetchNextPage,
        hasNextPage,
        status,
    } = useInfiniteQuery({
        queryKey: ['gallery'],
        queryFn: fetchGallery,
        initialPageParam: 1,
        getPreviousPageParam: (firstPage) => firstPage.previousId ?? undefined,
        getNextPageParam: (lastPage) => lastPage.nextId ?? undefined,
    });


    const images = useMemo(() => data?.pages.reduce((prev, page) => {
        return {
            previousId: page.previousId,
            nextId: page.nextId,
            data: [...prev.data, ...page.data]
        }
    }), [data])

    return status === 'pending' ? (
        <Loading/>
    ) : status === 'error' ? (
            <p>Error: {error.message}</p>
        ) :
        (
            <Container>
                <ImageList images={images} fetchNextPage={fetchNextPage} hasNextPage={hasNextPage}/>
            </Container>
        )
};

export default Gallery;