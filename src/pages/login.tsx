import {Button, Card, Container, Flex, Heading, TextField} from '@radix-ui/themes';
import {useFormik} from 'formik';
import {useHistory} from "react-router-dom";
import {capitalize} from '../shared/helpers/capitalize';
import {useUserStore} from '../store/userStore';
import {toast} from 'react-toastify';

const SignInForm = () => {
    const loginAction = useUserStore((state) => state.login);
    const {user, logged} = useUserStore();
    const history = useHistory();

    const validate = (values: { user: string; password: string; }) => {
        const passwordRegex = new RegExp(`^123${capitalize(values.user)}$`, 'gi');
        const errors: Partial<{ user: string, password: string }> = {};

        if (!values.user) {
            errors.user = 'campo requerido';
        } else if (!/^[a-z]+$/.test(values.user)) {
            errors.user = 'usuario incorrecto';
        }

        if (!values.password) {
            errors.password = 'campo requerido';
        } else if (!passwordRegex.test(values.password)) {
            errors.password = 'Contraseña invalida';
        }
        return errors;
    };

    const formik = useFormik({
        initialValues: {
            user: '',
            password: '',
        },
        onSubmit: values => {
            const {user} = values;
            toast("Login exitoso");
            loginAction(user);
            history.push("/gallery");
        },
        validate
    });
    if (user && logged) {
        history.push("/gallery");
    }

    const {errors, touched, dirty} = formik;

    return (
        <Container size="1">
            <Card>
                <form onSubmit={formik.handleSubmit}>
                    <Flex gap="3" direction="column" align="center" justify="center">
                        <Heading>Image Gallery</Heading>
                        {errors.user && touched.user && dirty && <div>{errors.user}</div>}
                        <TextField.Input
                            placeholder="Username"
                            id="user"
                            name="user"
                            type="text"
                            onChange={formik.handleChange}
                            value={formik.values.user}
                        />
                        {errors.password && touched.password && dirty && <div>{errors.password}</div>}
                        <TextField.Input
                            placeholder="Password"
                            id="password"
                            name="password"
                            type="password"
                            onChange={formik.handleChange}
                            value={formik.values.password}
                        />
                        <Button type='submit'>Submit</Button>
                    </Flex>
                </form>
            </Card>
        </Container>
    );
};

export default SignInForm;