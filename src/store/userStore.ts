import {create, StateCreator} from 'zustand';
import {persist, PersistOptions} from 'zustand/middleware'
import {Iimage} from '../shared/interfaces/images';

interface UserState {
    logged: boolean
    user: string | undefined
    favorites: Iimage[]
    login: (user: string) => void
    logout: () => void
    saveImage: (by: Iimage) => void
    removeImage: (by: Iimage) => void
    checkImage: (by: Iimage) => boolean
}

const initialState = {
    logged: false,
    user: undefined,
    favorites: [],
}

type MyPersist = (
    config: StateCreator<UserState>,
    options: PersistOptions<UserState>
) => StateCreator<UserState>

export const useUserStore = create<UserState>()(
    (persist as MyPersist)(
        (set, get) => ({
            ...initialState,
            login: (user: string) => set(() => ({...get(), user: user, logged: true})),
            logout: () => set(initialState),
            saveImage: (image: Iimage) => set(() => ({...get(), favorites: [...get().favorites, image]})),
            removeImage: (image: Iimage) => set(() => ({
                ...get(),
                favorites: get().favorites.filter(item => item.id !== image.id)
            })),
            checkImage: (image: Iimage) => get().favorites.some(favorite => favorite.id === image?.id),
        }),
        {
            name: 'image-gallery',
        }
    )
);