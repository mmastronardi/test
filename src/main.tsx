import React from 'react'
import ReactDOM from 'react-dom/client'
import {Theme} from '@radix-ui/themes';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import {
    QueryClient,
    QueryClientProvider,
} from '@tanstack/react-query';
import {ToastContainer} from 'react-toastify';
import PrivateRoute from './shared/components/privatePage.tsx';
import Login from './pages/login.tsx'
import Gallery from './pages/gallery.tsx';
import Details from './pages/detail.tsx';
import Favorites from './pages/favorites.tsx';
import './index.css';
import 'react-toastify/dist/ReactToastify.css';
import '@radix-ui/themes/styles.css';

const queryClient = new QueryClient();

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <Theme>
            <QueryClientProvider client={queryClient}>
                <Router>
                    <Switch>
                        <PrivateRoute path="/details/:id">
                            <Details/>
                        </PrivateRoute>
                        <PrivateRoute path="/gallery">
                            <Gallery/>
                        </PrivateRoute>
                        <PrivateRoute path="/favorites">
                            <Favorites/>
                        </PrivateRoute>
                        <Route path="/">
                            <Login/>
                        </Route>
                    </Switch>
                </Router>
                <ToastContainer/>
            </QueryClientProvider>
        </Theme>
    </React.StrictMode>,
)
