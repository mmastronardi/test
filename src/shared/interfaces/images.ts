interface Iimage {
    id: number,
    author: string,
    download_url: string,
}

interface IimageList {
    data: Iimage[];
    previousId?: number;
    nextId?: number;
}

export type {
    Iimage,
    IimageList
}