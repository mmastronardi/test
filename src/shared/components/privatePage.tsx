import {ReactElement, useState} from "react";
import {
    Route,
    Redirect,
    useHistory,
} from "react-router-dom";
import {slide as Menu} from 'react-burger-menu';
import {Avatar, Box, Button, Card, Flex, Text} from "@radix-ui/themes";
import {StarIcon, HomeIcon, LockClosedIcon} from '@radix-ui/react-icons';
import {useUserStore} from "../../store/userStore";


const PrivateRoute = ({children, ...rest}: { children: ReactElement; path: string }) => {
    const user = useUserStore((state) => state.user);
    const logout = useUserStore((state) => state.logout);
    const history = useHistory();
    const [isOpen, setIsOpen] = useState(false);

    const onGalleryClick = () => {
        setIsOpen(false);
        history.push("/gallery");
    };

    const onFavoriteClick = () => {
        setIsOpen(false);
        history.push("/favorites");
    };

    const onLogoutClick = () => {
        setIsOpen(false);
        logout();
    }

    const handleOnOpen = () => {
        setIsOpen(true);
    }

    const handleOnClose = () => {
        setIsOpen(false);
    }

    return (
        <Route
            {...rest}
            render={({location}) =>
                user ? (
                    <Box id="outer-container">
                        <Menu isOpen={isOpen} pageWrapId={"page-wrap"} outerContainerId={"outer-container"}
                              onOpen={handleOnOpen} onClose={handleOnClose}>
                            <Flex direction="column" gap="4" justify="between" className="nav-cust-height-95">
                                <Card className="username-card" style={{maxWidth: 240}}>
                                    <Flex gap="3" align="center">
                                        <Avatar
                                            size="3"
                                            src="https://static.change.org/profile-img/default-user-profile.svg"
                                            radius="full"
                                            fallback={user}
                                        />
                                        <Box>
                                            <Text as="div" size="2" weight="bold">
                                                {user}
                                            </Text>
                                        </Box>
                                    </Flex>
                                </Card>
                                <Flex direction="column" gap="4" justify="between" className="nav-cust-height-95">
                                    <Flex direction="column" gap="4">
                                        <Button onClick={onGalleryClick}>
                                            <HomeIcon width="16" height="16"/> Inicio
                                        </Button>
                                        <Button onClick={onFavoriteClick}>
                                            <StarIcon width="16" height="16"/> Mis Imagenes
                                        </Button>
                                    </Flex>
                                    <Flex direction="column" gap="4" justify="end">
                                        <Button onClick={onLogoutClick}>
                                            <LockClosedIcon width="16" height="16"/> Cerrar Sesión
                                        </Button>
                                    </Flex>
                                </Flex>
                            </Flex>
                        </Menu>
                        <Box id="page-wrap">
                            {children}
                        </Box>
                    </Box>
                ) : (
                    <Redirect
                        to={{
                            pathname: "/",
                            state: {from: location}
                        }}
                    />
                )
            }
        />
    );
}

export default PrivateRoute;