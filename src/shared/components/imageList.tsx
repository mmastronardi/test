import InfiniteScroll from 'react-infinite-scroll-component';
import {Grid} from '@radix-ui/themes';
import {Iimage, IimageList} from '../interfaces/images';
import {Loading} from './loader';
import ImageCard from './imageCard';

interface IImageListProps {
    images?: IimageList;
    fetchNextPage: () => void;
    hasNextPage: boolean;
}


const ImageList = ({images, fetchNextPage, hasNextPage}: IImageListProps) => {
    return (
        <InfiniteScroll
            dataLength={images ? images.data.length : 0}
            next={() => fetchNextPage()}
            hasMore={hasNextPage}
            loader={<Loading/>}
        >
            <Grid columns="3" gap="3" width="auto">
                {images && images.data.map((image: Iimage) => (
                    <ImageCard image={image} key={image.id}/>
                ))}
            </Grid>
        </InfiniteScroll>
    )
}

export default ImageList;