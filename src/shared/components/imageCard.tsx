import {Link} from 'react-router-dom';
import {AlertDialog, Box, Button, Card, Flex, Text} from '@radix-ui/themes';
import {StarFilledIcon, StarIcon} from '@radix-ui/react-icons';
import {Iimage} from '../interfaces/images';
import {useUserStore} from '../../store/userStore';

const ImageCard = ({image}: { image: Iimage }) => {
    const {checkImage, saveImage, removeImage} = useUserStore();
    const isFavorite = checkImage(image);
    const onFavoriteClick = (e: { preventDefault: () => void; }) => {
        e.preventDefault();
        if (isFavorite) {
            removeImage(image);
        } else {
            saveImage(image);
        }
    };

    return (
        <Card size="3" asChild style={{maxWidth: 350}}>
            <Box>
                <Flex direction="column" gap="3">
                    <Link className='link-hidden' to={`/details/${image.id}`}>
                        <Flex direction="column" align="center" gap="3">
                            <img
                                src={image.download_url}
                                alt={`image from ${image.author}`}
                                style={{
                                    objectFit: 'cover',
                                    width: '100%',
                                    height: '100%',
                                    borderRadius: 'var(--radius-2)',
                                }}
                            />
                            <Text as="div" size="2" weight="bold">
                                {image.author}
                            </Text>
                        </Flex>
                    </Link>
                    <Flex justify="center">
                        {isFavorite ? (
                            <AlertDialog.Root>
                                <AlertDialog.Trigger>
                                    <Button>
                                        <StarFilledIcon width="16" height="16"/>
                                    </Button>
                                </AlertDialog.Trigger>
                                <AlertDialog.Content style={{maxWidth: 500}}>
                                    <AlertDialog.Title>Quitar de favoritos</AlertDialog.Title>
                                    <Flex direction="column" gap="4">
                                        <AlertDialog.Description size="2">
                                            ¿Estás seguro de que deseas eliminar esta imagen de favoritos?
                                        </AlertDialog.Description>
                                        <Flex gap="3" justify="end">
                                            <AlertDialog.Cancel>
                                                <Button variant="soft" color="gray">Cancelar</Button>
                                            </AlertDialog.Cancel>
                                            <AlertDialog.Action>
                                                <Button color="red" onClick={onFavoriteClick}>Eliminar</Button>
                                            </AlertDialog.Action>
                                        </Flex>
                                    </Flex>
                                </AlertDialog.Content>
                            </AlertDialog.Root>
                        ) : (
                            <Button onClick={onFavoriteClick}>
                                <StarIcon width="16" height="16"/>
                            </Button>
                        )}
                    </Flex>
                </Flex>
            </Box>
        </Card>
    )
}

export default ImageCard;