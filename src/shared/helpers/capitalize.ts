export const capitalize = ([firstLetter, ...restOfWord]: string): string =>
    firstLetter.toUpperCase() + restOfWord.join("");